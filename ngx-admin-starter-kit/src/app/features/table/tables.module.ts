import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NbCardModule, NbIconModule, NbInputModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';

import { TableComponent } from './pages/table/table.component';
import { SmartTableComponent } from './pages/smart-table/smart-table.component';
import { SmartTableService } from './services/smart-table.service';

const routes: Routes = [{
  path: '',
  component: TableComponent,
  },
  {
    path: 'table',
    component: TableComponent,
  },
  {
    path: 'smart-table',
    component: SmartTableComponent,
  },
];

@NgModule({
  declarations: [TableComponent, SmartTableComponent],
  imports: [
    NbCardModule,
    NbIconModule,
    NbInputModule,
    Ng2SmartTableModule,
    ThemeModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
  providers: [SmartTableService],
})
export class TablesModule { }
