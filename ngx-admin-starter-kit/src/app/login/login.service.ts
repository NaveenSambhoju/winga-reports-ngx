import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppStore } from 'app/@core/store/app-store.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  apiURL = environment.apiURL;
  constructor(private http: HttpClient, private appStore: AppStore, private router: Router) { }

  login(username, password) {
    this.http.post<any>(`${this.apiURL}/auth/user/login`, { username, password })
        .subscribe( user => {
            localStorage.setItem('user', JSON.stringify(user));
            this.appStore.user = user.user;
            this.appStore.token = user.tokens['access-token'];
            this.router.navigate(['pages/dashboard']);
        });
}

  logout() {
    localStorage.clear();
    this.router.navigate(['pages/login']);
  }
}
