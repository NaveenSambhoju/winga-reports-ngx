import { Component, OnInit } from '@angular/core';
import { AppStore } from 'app/@core/store/app-store.service';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService, NbWindowService } from '@nebular/theme';

@Component({
  selector: 'winga-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit {

  constructor(public appStore: AppStore) {}

  ngOnInit(): void {
  }

}
