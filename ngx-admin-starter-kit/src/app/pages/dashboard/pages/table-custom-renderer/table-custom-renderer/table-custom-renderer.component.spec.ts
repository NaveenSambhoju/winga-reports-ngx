import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableCustomRendererComponent } from './table-custom-renderer.component';

describe('TableCustomRendererComponent', () => {
  let component: TableCustomRendererComponent;
  let fixture: ComponentFixture<TableCustomRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableCustomRendererComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableCustomRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
