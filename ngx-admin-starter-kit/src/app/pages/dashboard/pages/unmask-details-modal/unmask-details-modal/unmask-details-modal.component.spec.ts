import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnmaskDetailsModalComponent } from './unmask-details-modal.component';

describe('UnmaskDetailsModalComponent', () => {
  let component: UnmaskDetailsModalComponent;
  let fixture: ComponentFixture<UnmaskDetailsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnmaskDetailsModalComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnmaskDetailsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
