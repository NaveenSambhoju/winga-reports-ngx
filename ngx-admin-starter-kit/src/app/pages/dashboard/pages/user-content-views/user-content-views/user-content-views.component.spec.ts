import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserContentViewsComponent } from './user-content-views.component';

describe('UserContentViewsComponent', () => {
  let component: UserContentViewsComponent;
  let fixture: ComponentFixture<UserContentViewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserContentViewsComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContentViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
